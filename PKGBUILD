# Maintainer: Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Tom Newsom <Jeepster@gmx.co.uk>
# Contributor: Paul Mattal <paul@archlinux.org>

# ALARM: Kevin Mihelich <kevin@archlinuxarm.org>
#  - use -fPIC in host cflags for v7/v8 to fix print_options.c compile
#  - remove makedepends on ffnvcodec-headers, remove --enable-nvenc, --enable-nvdec
#  - remove depends on aom, remove --enable-libaom
#  - remove depends on intel-media-sdk, remove --enable-libmfx
#  - remove depends on vmaf, remove --enable-vmaf
#  - remove depends on rav1e, remove --enable-librav1e
#  - remove depends on svt-av1, remove --enable-libsvtav1
#  - remove --enable-lto

pkgname=ffmpeg-m2m
_pkgname=rpi-ffmpeg
pkgver=4.4
pkgrel=3
epoch=1
_commit=dde8d3c8f3cc279b9b92ed4f10a2e3990f4aadeb
pkgdesc='Complete solution to record, convert and stream audio and video'
arch=(aarch64)
url=https://ffmpeg.org/
license=(GPL3)
depends=(
  alsa-lib
  bzip2
  fontconfig
  fribidi
  gmp
  gnutls
  gsm
  jack
  lame
  libass.so
  libavc1394
  libbluray.so
  libdav1d.so
  libdrm
  libfreetype.so
  libiec61883
  libmodplug
  libpulse
  libraw1394
  librsvg-2.so
  libsoxr
  libssh
  libtheora
  libva.so
  libva-drm.so
  libva-x11.so
  libvdpau
  libvidstab.so
  libvorbisenc.so
  libvorbis.so
  libvpx.so
  libwebp
  libx11
  libx264.so
  libx265.so
  libxcb
  libxext
  libxml2
  libxv
  libxvidcore.so
  libzimg.so
  opencore-amr
  openjpeg2
  opus
  sdl2
  speex
  srt
  v4l-utils
  xz
  zlib
)
makedepends=(
  amf-headers
  avisynthplus
  clang
  git
  ladspa
  nasm
)
optdepends=(
  'avisynthplus: AviSynthPlus support'
  'ladspa: LADSPA filters'
)
provides=(
  ffmpeg
  libavcodec.so
  libavdevice.so
  libavfilter.so
  libavformat.so
  libavutil.so
  libpostproc.so
  libswresample.so
  libswscale.so
)
conflicts=(ffmpeg)
options=(
  debug
)
source=(
  "https://github.com/jc-kynesim/${_pkgname}/archive/${_commit}.tar.gz"
  "https://raw.githubusercontent.com/LibreELEC/LibreELEC.tv/master/packages/multimedia/ffmpeg/patches/libreelec/ffmpeg-001-libreelec.patch"
  "https://raw.githubusercontent.com/LibreELEC/LibreELEC.tv/master/packages/multimedia/ffmpeg/patches/dav1d/ffmpeg-support-dav1d-1-0-0.patch"

)
b2sums=('3f3e6f145ef4a8748af3597d9defe6e17c066058adeb6e95ebaa739df8455a631658dcb77208a0ed5d35a96cfbfbf570d95a9bfd1dd00879ad27c7f0ece999be'
        'd0fccf3f9f3131b583ae26a46933683adbe30b928bf2b6e8ff4d1a4b211a3126bfc4d55e7f37c5a37c38000a98de72226134bf3624be6cabb0ce75a88b317b43'
        '9f209ec0d18cba184378a6dd8199201529cdd43ea2d845d54467d5defbc1962a861c165b5e02758de5da5f8b7e98cebdd57345de5a2457a8d3079b6832b539b7')

prepare() {
  cd ${_pkgname}-${_commit}
 # git cherry-pick -n 988f2e9eb063db7c1a678729f58aab6eba59a55b # fix nvenc on older gpus
  #patch -Np1 -i ../ffmpeg-vmaf2.x.patch # vmaf 2.x support
  #patch -Np1 -i ../add-av_stream_get_first_dts-for-chromium.patch # https://crbug.com/1251779
  patch -Np1 -i ../ffmpeg-001-libreelec.patch
  patch -Np1 -i ../ffmpeg-support-dav1d-1-0-0.patch
#  echo "Nothing to prepare"
}

build() {
  cd ${_pkgname}-${_commit}

  [[ $CARCH == "armv7h" || $CARCH == "aarch64" ]] && CONFIG='--host-cflags="-fPIC"'

  ./configure \
    --prefix=/usr \
    --disable-debug \
    --disable-static \
    --disable-stripping \
    --enable-amf \
    --enable-avisynth \
    --enable-cuda-llvm \
    --enable-fontconfig \
    --enable-gmp \
    --enable-gnutls \
    --enable-gpl \
    --enable-ladspa \
    --enable-libass \
    --enable-libbluray \
    --enable-libdav1d \
    --enable-libfreetype \
    --enable-libfribidi \
    --enable-libgsm \
    --enable-libiec61883 \
    --enable-libjack \
    --enable-libmodplug \
    --enable-libmp3lame \
    --enable-libopencore_amrnb \
    --enable-libopencore_amrwb \
    --enable-libopenjpeg \
    --enable-libopus \
    --enable-libpulse \
    --enable-librsvg \
    --enable-libsoxr \
    --enable-libspeex \
    --enable-libsrt \
    --enable-libssh \
    --enable-libtheora \
    --enable-libv4l2 \
    --enable-libvidstab \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libwebp \
    --enable-libx264 \
    --enable-libx265 \
    --enable-libxcb \
    --enable-libxml2 \
    --enable-libxvid \
    --enable-libzimg \
    --enable-shared \
    --enable-version3 \
    --enable-v4l2_m2m \
    --enable-libdrm \
    --enable-hwaccels \
    $CONFIG

  make
  make tools/qt-faststart
  make doc/ff{mpeg,play}.1
}

package() {
  make DESTDIR="${pkgdir}" -C ${_pkgname}-${_commit} install install-man
  install -Dm 755 ${_pkgname}-${_commit}/tools/qt-faststart "${pkgdir}"/usr/bin/
}

# vim: ts=2 sw=2 et:
